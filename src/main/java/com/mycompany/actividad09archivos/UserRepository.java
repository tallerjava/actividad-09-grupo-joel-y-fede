package com.mycompany.actividad09archivos;

import java.io.IOException;
import java.io.*;

public class UserRepository {

    boolean buscador = false;

    public void GuardarUsuario(Usuario usuario) throws IOException {
        File archivo = new File("miarchivo.txt");
        if (!archivo.exists()) {
            /* Si el archivo no existe, lo crea */
            try {
                archivo.createNewFile();
                BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
                /* Se crea el "escritor" del archivo */
                bw.write(usuario.getUser() + "|" + usuario.getPass());
                bw.close();
                System.out.println("Se guardo el nuevo usuario: " + usuario.getUser());
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            FileReader fichero = new FileReader(archivo);
            /* Se crea un fichero de lectura*/
            BufferedReader br = new BufferedReader(fichero);
            /* Se crea un "lector" de ficheros*/
            try {
                String line;
                while (!buscador && (line = br.readLine()) != null) {
                    /* Se busca en todas las lineas si existe el usuario cargado*/
                    if (!buscador && line.split("\\|")[0].equals(usuario.getUser())) {
                        buscador = true;
                        System.out.println("El usuario: " + usuario.getUser() + " ya existe");
                    }
                }
                if (!buscador) {
                    /* Si buscador es falso, se carga el usuario utilizando al escritor */
                    try {
                        BufferedWriter bw = new BufferedWriter(new FileWriter(archivo, true));
                        bw.append("\r\n");
                        /* es un salto de linea en la escritura*/
                        bw.write(usuario.getUser() + "|" + usuario.getPass());
                        System.out.println("Se guardo el nuevo usuario: " + usuario.getUser());
                        bw.close();
                    } catch (IllegalArgumentException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}