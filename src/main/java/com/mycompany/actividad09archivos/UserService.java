package com.mycompany.actividad09archivos;

import java.io.IOException;

public class UserService {

    private int min;
    private int max;
    
    public void validarPassword(String password) throws IllegalArgumentException {
        min = 6;
        max = 25;
        if (password == null || password.equals("")) {
            throw new IllegalArgumentException("La password ingresada es nula o vacio");
        }
        if (password.length() < min || !password.matches("^[^|]*[0-9][^|]*$")) {/* si es menor y tiene digito*/
            throw new IllegalArgumentException("La password no cuenta con los caracteres minimos esperados o utiliza caracteres invalidos(|)");
        }
        if (password.length() > max || !password.matches("^[^|]*[0-9][^|]*$")) {/* si es mayor y tiene digito*/
            throw new IllegalArgumentException("La password contiene m�s caracteres de los esperados o utiliza caracteres invalidos(|)");
        }
    }

    public void validarUser(String user) throws IllegalArgumentException {
        min = 4;
        max = 8;
        if (user == null || user.equals("")) {
            throw new IllegalArgumentException("El usuario ingresado es nulo o vacio");
        }
        if (user.length() < min || user.length() > max || !user.matches("^[^|]*$")) {
            throw new IllegalArgumentException("El usuario ingresado no cumple con condiciones validas");
        }
    }

    public void guardarUsuario(Usuario usuario) throws IOException {
        UserRepository Archivo = new UserRepository();
        Archivo.GuardarUsuario(usuario);
    }
}
