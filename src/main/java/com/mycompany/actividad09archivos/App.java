package com.mycompany.actividad09archivos;
import java.io.*;

public class App {

    public static void main(String[] arg) throws IOException {
        try {
            Usuario user = new Usuario("usDSa13", "asdasd1");
            UserService servicioUsuario = new UserService();
            servicioUsuario.validarUser(user.getUser());
            servicioUsuario.validarPassword(user.getPass());
            servicioUsuario.guardarUsuario(user);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
}

