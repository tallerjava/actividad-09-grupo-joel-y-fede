import com.mycompany.actividad09archivos.UserService;
import java.io.*;
import org.junit.Test;

public class UserServiceTest {

    UserService ServicioUsuario = new UserService();

    public UserServiceTest() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordNull_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarPassword(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_Passwordvacio_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarPassword("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordSinCaracteresMinimos_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarPassword("asdasd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordConMasCaracteresDeLosPermitidos_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarPassword("asdasdasdasdasdasdasdasdasdasdasdasd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordSinDigitos_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarPassword("asdasdasdasd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_PasswordPaswordConCaracterInvalido_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarPassword("asdasdas1|");
    }
    @Test(expected = IllegalArgumentException.class)
    public void validarPassword_ValidarUser_IllegalArgumentException() throws IllegalArgumentException {
        ServicioUsuario.validarUser("asda|");
    }
    
}
